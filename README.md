# Instructions
- run a postgre server
- execute the `import-users.sql` and `import-users-tests.sql` scripts in order to generate the production and test databases.
- import the project into Eclipse
- run the UserCrudApplication
