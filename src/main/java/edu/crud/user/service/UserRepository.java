package edu.crud.user.service;

import org.springframework.data.repository.CrudRepository;

import edu.crud.user.domain.User;

public interface UserRepository extends CrudRepository<User, String> {
	// blank
}
