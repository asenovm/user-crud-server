package edu.crud.user.web;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.crud.user.domain.User;
import edu.crud.user.service.UserRepository;

@Controller
@RequestMapping("/users")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(method = GET)
	@ResponseBody
	public Iterable<User> fetchUsers() {
		return userRepository.findAll();
	}

	@RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
	@ResponseBody
	public User createUser(@RequestBody Map<String, String> payload, HttpServletResponse response) {
		final String firstName = payload.get(RequestKey.FIRST_NAME);
		final String lastName = payload.get(RequestKey.LAST_NAME);
		final String email = payload.get(RequestKey.EMAIL);
		Date birthDate = null;

		final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			birthDate = format.parse(payload.get(RequestKey.BIRTH_DATE));
		} catch (ParseException e) {
			response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
			return null;
		}

		final User user = new User(firstName, lastName, email, birthDate);
		return userRepository.save(user);
	}

	@RequestMapping(value = "{email}", method = DELETE)
	@ResponseBody
	public void deleteUser(@PathVariable String email) {
		userRepository.delete(email);
	}

}
