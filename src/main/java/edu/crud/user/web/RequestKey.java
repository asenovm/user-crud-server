package edu.crud.user.web;

public class RequestKey {

	/**
	 * {@value}
	 */
	public static final String FIRST_NAME = "firstName";

	/**
	 * {@value}
	 */
	public static final String LAST_NAME = "lastName";

	/**
	 * {@value}
	 */
	public static final String BIRTH_DATE = "birthDate";

	/**
	 * {@value}
	 */
	public static final String EMAIL = "email";
}
