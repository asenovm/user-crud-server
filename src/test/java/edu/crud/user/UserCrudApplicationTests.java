package edu.crud.user;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import edu.crud.user.domain.User;
import edu.crud.user.service.UserRepository;
import edu.crud.user.web.RequestKey;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(UserCrudApplication.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:test.properties")
public class UserCrudApplicationTests {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private UserRepository repository;

	private MockMvc mvc;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		repository.deleteAll();
	}

	@After
	public void tearDown() {
		repository.deleteAll();
	}

	@Test
	public void testGet() throws Exception {
		this.mvc.perform(get("/users")).andExpect(status().isOk()).andExpect(content().string("[]"));
	}

	@Test
	public void testPost() throws Exception {
		final User mockUser = MockUser.create();
		final JSONObject content = new JSONObject();
		content.put(RequestKey.FIRST_NAME, mockUser.getFirstName());
		content.put(RequestKey.LAST_NAME, mockUser.getLastName());
		content.put(RequestKey.EMAIL, mockUser.getEmail());
		final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		content.put(RequestKey.BIRTH_DATE, format.format(mockUser.getBirthDate()));

		final MvcResult result = this.mvc.perform(MockMvcRequestBuilders.post("/users")
				.contentType(MediaType.APPLICATION_JSON).content(content.toString())).andReturn();

		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

		final JSONObject response = new JSONObject(result.getResponse().getContentAsString());
		Assert.assertEquals(mockUser.getFirstName(), response.get(RequestKey.FIRST_NAME));
		Assert.assertEquals(mockUser.getLastName(), response.get(RequestKey.LAST_NAME));
		Assert.assertEquals(mockUser.getEmail(), response.get(RequestKey.EMAIL));
		Assert.assertEquals(mockUser.getBirthDate().getTime(), response.getLong(RequestKey.BIRTH_DATE));
	}

	@Test
	public void testDelete() throws Exception {
		testPost();

		final User mockUser = MockUser.create();
		this.mvc.perform(MockMvcRequestBuilders.delete("/users/" + mockUser.getEmail())).andExpect(status().isOk());

		testGet();
	}

}
