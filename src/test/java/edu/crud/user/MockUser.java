package edu.crud.user;

import java.util.Date;

import edu.crud.user.domain.User;

public class MockUser extends User {

	/**
	 * {@value}
	 */
	private static final long serialVersionUID = 5479907172550566135L;

	private static final String TEST_FIRST_NAME = "test";

	private static final String TEST_LAST_NAME = "test";

	private static final String TEST_EMAIL = "email@test.com";

	@SuppressWarnings("deprecation")
	private static final Date TEST_BIRTH_DATE = new Date(90, 1, 1);

	public static User create() {
		return new User(TEST_FIRST_NAME, TEST_LAST_NAME, TEST_EMAIL, TEST_BIRTH_DATE);
	}

}
