package edu.crud.user.service;

import java.util.Collection;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import edu.crud.user.MockUser;
import edu.crud.user.UserCrudApplication;
import edu.crud.user.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(UserCrudApplication.class)
@TestPropertySource(locations = "classpath:test.properties")
@WebAppConfiguration
public class UserRepositoryIntegrationTests {

	@Autowired
	UserRepository userRepository;

	@Before
	public void setUp() {
		userRepository.deleteAll();
	}

	@After
	public void tearDown() {
		userRepository.deleteAll();
	}

	@Test
	public void testCreateRead() {
		final User user = MockUser.create();
		userRepository.save(user);

		final Iterable<User> users = userRepository.findAll();
		final Collection<User> usersCollection = iterableToCollection(users);
		Assert.assertEquals(usersCollection.size(), 1);
	}

	@Test
	public void testCreateReadDelete() {
		final User user = MockUser.create();
		userRepository.save(user);

		userRepository.delete(user.getEmail());

		final Iterable<User> users = userRepository.findAll();
		final Collection<User> usersCollection = iterableToCollection(users);
		Assert.assertEquals(usersCollection.size(), 0);
	}

	private <T> Collection<T> iterableToCollection(Iterable<T> iterable) {
		final Collection<T> result = new LinkedList<>();
		for (T current : iterable) {
			result.add(current);
		}
		return result;
	}
}
